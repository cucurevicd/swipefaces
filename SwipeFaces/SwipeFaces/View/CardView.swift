//
//  CardView.swift
//  SwipeFaces
//
//  Created by Dusan Cucurevic on 7/16/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit

class CardView: UIView {

    @IBOutlet weak var developerImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var skillLabel: UILabel!
    
    func config(dev: Developer){
        
        self.developerImage.image = dev.sex == .male ? #imageLiteral(resourceName: "male") : #imageLiteral(resourceName: "female")
        self.usernameLabel.text = dev.name
        self.skillLabel.text = dev.skill
    }

}
