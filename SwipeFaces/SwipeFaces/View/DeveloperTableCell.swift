//
//  DeveloperTableCell.swift
//  SwipeFaces
//
//  Created by Dusan Cucurevic on 7/16/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit

class DeveloperTableCell: UITableViewCell {

    var developer: Developer?{
        didSet{
            if developer != nil{
                self.configUI()
            }
        }
    }
    @IBOutlet weak var developerImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var skillLabel: UILabel!
    
    func config(dev: Developer?){
        
        self.developer = dev
    }
    
    
    private func configUI(){
        
        self.developerImage.image = developer!.sex == .male ? #imageLiteral(resourceName: "male") : #imageLiteral(resourceName: "female")
        self.usernameLabel.text = developer!.name
        self.skillLabel.text = developer!.skill
    }
}
