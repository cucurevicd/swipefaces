//
//  Developer.swift
//  SwipeFaces
//
//  Created by Dusan Cucurevic on 7/16/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

enum SexEnum : String, Decodable {
    
    case female = "F"
    case male = "M"
    
}

struct Developer : Decodable {
    
    var name: String
    var skill: String
    var sex: SexEnum
    var like: Bool?
}
