//
//  ListViewController.swift
//  SwipeFaces
//
//  Created by Dusan Cucurevic on 7/16/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    var developers: [Developer]!
    var filterDevelopers : [Developer]{
        return developers.filter{ $0.like != nil}.filter{ $0.like! == (segmented.selectedSegmentIndex == 0)}
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmented: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
    }
    
    
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        
        self.tableView.reloadData()
    }
}

extension ListViewController : UITableViewDataSource{
 
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterDevelopers.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "DeveloperTableCell", for: indexPath) as! DeveloperTableCell
        cell.config(dev: filterDevelopers[indexPath.row])
        
        return cell
    }
}
