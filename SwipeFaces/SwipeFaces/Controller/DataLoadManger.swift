//
//  DataLoadManger.swift
//  SwipeFaces
//
//  Created by Dusan Cucurevic on 7/16/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

class DataLoaderManager{
    
    static func getData() -> [Developer]?{
        
        let bundle = Bundle.main
        let path = bundle.path(forResource: "developer_names", ofType: "json")
        
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: Data.ReadingOptions.alwaysMapped)
            let decoder = JSONDecoder()
            return try? decoder.decode([Developer].self, from: data)
        }
        catch{
            print(error)
            fatalError()
        }
    }

}
