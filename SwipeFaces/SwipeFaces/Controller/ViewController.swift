//
//  ViewController.swift
//  SwipeFaces
//
//  Created by Dusan Cucurevic on 7/16/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit
import Koloda

class ViewController: UIViewController {

    @IBOutlet weak var swipeView: KolodaView!
    var developers: [Developer] = [Developer](){
        
        didSet{
            self.swipeView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.swipeView.delegate = self
        self.swipeView.dataSource = self
        
        loadData()
    }

    private func loadData(){
        
        if let devs = DataLoaderManager.getData(){
            self.developers = devs
        }
        else{
            let errorVC = UIAlertController(title: NSLocalizedString("Error", comment: "Error title"), message: NSLocalizedString("No data to load", comment: ""), preferredStyle: .alert)
            self.present(errorVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func refreshAction(_ sender: Any) {
        let alertVC = UIAlertController(title: "Alert", message: "Are you sure you want to refresh all developers?. All likes will be lost", preferredStyle: .actionSheet)
        
        let yesAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (action) in
            
            DispatchQueue.main.async {
                self.loadData()
            }
        }
        let noAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil)
        alertVC.addAction(yesAction)
        alertVC.addAction(noAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "listSegue"{
            
            let vc = segue.destination as! ListViewController
            vc.developers = developers
        }
    }
}

extension ViewController : KolodaViewDataSource{
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        let card = Bundle.main.loadNibNamed("CardView", owner: self, options: nil)![0] as! CardView
        card.config(dev: developers[index])
        
        return card
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return developers.count
    }
    
}

extension ViewController: KolodaViewDelegate{
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        
        let errorVC = UIAlertController(title: NSLocalizedString("Alert", comment: "Error title"), message: NSLocalizedString("No more developers", comment: ""), preferredStyle: .alert)
        self.present(errorVC, animated: true, completion: nil)
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        self.developers[index].like = direction == .right
    }
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
}

